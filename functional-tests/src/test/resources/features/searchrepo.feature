  Feature: As an api user I want to test a REST API,
  				 Users should be able to submit GET request to a web service
  
    Scenario: Get list of repositories created last week
      When I get the service uri /search/5
      Then the service uri returns status code 200
      And the content type is json
      And the body has json path $.[0] of type object
      And the body has json path $.[0].name of type string
			And the body has json path $.[0].html_url of type string
      And the body has json path $.[0].description of type string
      And the body has json path $.[0].watchers_count of type numeric
      And the body has json path $.[0].language of type string
      
      



