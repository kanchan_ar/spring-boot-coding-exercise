package com.telstra.codechallenge.searchrepo.service;

import com.telstra.codechallenge.searchrepo.model.SearchRepo;

public interface ListRepoServiceInterface {

	SearchRepo findReposCreated(int repoNumber);
	SearchRepo findAllReposCreated();
}
