package com.telstra.codechallenge.searchrepo.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.searchrepo.model.Item;
import com.telstra.codechallenge.searchrepo.exception.ListRepositoryException;
import com.telstra.codechallenge.searchrepo.model.SearchRepo;
import com.telstra.codechallenge.searchrepo.service.ListRepoServiceInterface;
import com.telstra.codechallenge.searchrepo.service.ListRepositoryService;


@RestController
public class ListRepositoryController {

	@Autowired
	private ListRepoServiceInterface listRepositoryService;
	
	private Logger logger = Logger.getLogger(ListRepositoryController.class);

	public ListRepositoryController(
			ListRepositoryService listRepoCreatedService) {
    
	}

	/**
	 * API method to search N number(parameter passed) of GIT repositories created in the last week
	 * 
	 * @param number
	 * @return
	 */
	  @RequestMapping(path = "/search/{number}", method = RequestMethod.GET)
	  public List<Item> repoCreated(@PathVariable int number)  throws ListRepositoryException{ 
		  List<Item> items = null;
		  try {
			  SearchRepo hottestRepos = listRepositoryService.findReposCreated(number);
			  Integer totalCount = hottestRepos.getTotalCount();
			  items = hottestRepos.getItems();
		      logger.info("Using GIT API to get a list of the highest starred repositories in the last week. Total count:"+ totalCount);
		  
		  }catch(Exception e) {
			  logger.error("Error while fectching the records from GIT API: "+ e.getMessage());
		  }
		 
		  return items;
	  }
	  
	  /**
	   * 
	   * API method to get list of all GIT repositories created in the last week
	   * 
	   * @return ResponseEntity<SearchRepo>
	   */
	  @RequestMapping(path = "/search", method = RequestMethod.GET, produces = "application/json")
	  public SearchRepo repoCreated() { 
		  SearchRepo hottestRepos = listRepositoryService.findAllReposCreated();
		  return hottestRepos;
	  }

	 
	  @RequestMapping(path = "/", method = RequestMethod.GET)
	  public String defaultAPI() { 
		  return "Spring Boot Application. Endpoint created for external API call";
	  }

}

