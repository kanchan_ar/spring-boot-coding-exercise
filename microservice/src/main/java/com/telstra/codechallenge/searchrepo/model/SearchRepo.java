package com.telstra.codechallenge.searchrepo.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchRepo {

    @JsonProperty("total_count")
    private Integer totalCount;
  
    @JsonProperty("items")
    private List<Item> items = null;

	public Integer getTotalCount() {
		return totalCount;
	}
	
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	
	public List<Item> getItems() {
		return items;
	}
	
	public void setItems(List<Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "SearchRepo [totalCount=" + totalCount + ", items=" + items + "]";
	}
  

 
}
