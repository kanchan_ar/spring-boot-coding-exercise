package com.telstra.codechallenge.searchrepo.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ListRepositoryException extends Exception {

	private static final long serialVersionUID = -470180507998010368L;

	public ListRepositoryException() {
		super();
	}

	public ListRepositoryException(final String message) {
		super(message);
	}
	
    @ExceptionHandler(value=NumberFormatException.class)
    public ResponseEntity<Object> handleNumberFormatException(NumberFormatException ex)
    {
        return new ResponseEntity("Pass Numeric value in the URL to fetch number of records", HttpStatus.BAD_REQUEST);
    }
    
    
}