package com.telstra.codechallenge.searchrepo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Item {
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("html_url")
	private String htmlUrl;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("watchers_count")
	private Integer watchersCount;
	
	@JsonProperty("language")
	private String language;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHtmlUrl() {
		return htmlUrl;
	}

	public void setHtmlUrl(String htmlUrl) {
		this.htmlUrl = htmlUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (this.description == null) {
			this.description =  "";
		} else
			this.description = description;
	}

	public Integer getWatchersCount() {
		return watchersCount;
	}

	public void setWatchersCount(Integer watchersCount) {
		this.watchersCount = watchersCount;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		if(this.language == null) 
			this.language="";
		else
			this.language = language;
	}



}
