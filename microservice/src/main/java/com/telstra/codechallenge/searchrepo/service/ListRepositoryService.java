package com.telstra.codechallenge.searchrepo.service;

import java.time.LocalDate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.searchrepo.model.SearchRepo;

@Service
public class ListRepositoryService implements ListRepoServiceInterface{

	@Value("${repo.base.url}")
    private String baseUrl;

    private RestTemplate restTemplate;
    
    private Logger logger = Logger.getLogger(ListRepositoryService.class);
  
    private static String QUERY_PARAMETER_CREATED = "/search/repositories?q=created:>";
    private static String QUERY_PARAMETER_SORT = "&sort=stars&order=desc";
    private static String QUERY_PARAMETER_NUMBER = "&per_page=";
  
    public ListRepositoryService(RestTemplate restTemplate) {
    	 this.restTemplate = restTemplate;
    }

    /**
	 * Get last week's Date
	*/
    LocalDate now = LocalDate.now();
    LocalDate lastWeek = now.minusDays(7);
    
    @Override
	public SearchRepo findReposCreated(int repoNumber) {
		String searchRepoBaseUrl = baseUrl + QUERY_PARAMETER_CREATED + lastWeek  + QUERY_PARAMETER_SORT + QUERY_PARAMETER_NUMBER + repoNumber;	
		logger.info("GIT API URL------"+searchRepoBaseUrl);
		return restTemplate.getForObject(searchRepoBaseUrl, SearchRepo.class);
	}

    @Override
	public SearchRepo findAllReposCreated() {
		String searchRepoBaseUrl = baseUrl + QUERY_PARAMETER_CREATED + lastWeek  + QUERY_PARAMETER_SORT;	
		logger.info("GIT API URL------"+searchRepoBaseUrl);
		final ResponseEntity<SearchRepo> responseEntity = restTemplate.getForEntity(searchRepoBaseUrl, SearchRepo.class);
		//restTemplate.getForObject(searchRepoBaseUrl, SearchRepo.class);
		return responseEntity.getBody();
	}


}
