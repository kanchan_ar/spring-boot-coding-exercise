# Telstra Spring Boot Coding Exercise #

## Structure ##

The `micoservice` module produces a spring boot application. 'hello world' and 'quotes' have the existing sample code.
'searchrepo' folder has code for the exercise.
The `functional-tests` module starts the spring boot application defined in the `microservice` module and runs cucumber 
tests defined in`functional-tests/src/test/resources/features` to verify behaviour.

The step definitions for the cucumber tests are defined in 
`functional-tests/src/test/java/com.telstra.codechallenge.functionaltests/steps/ServiceStepDefinitions.java`.

## Exercise ##

### Find the hottest repositories created in the last week ###

Use the [GitHub API][1] to expose an endpoint in this microservice the will get a list of the 
highest starred repositories in the last week.

The endpoint should accept a parameter that sets the number of repositories to return.

The following fields should be returned:

      html_url
      watchers_count
      language
      description
      name

Cucumber tests should be used to verify the behaviour

## Requirements

For building and running the application you need:

* 	[Maven](https://maven.apache.org/) - Dependency Management
* 	[JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java™ Platform, Standard Edition Development Kit 
* 	[Spring Boot](https://spring.io/projects/spring-boot) - Framework to ease the bootstrapping and development of new Spring Applications
* 	[git](https://git-scm.com/) - Free and Open-Source distributed version control system 

## External Tools Used

* [Postman](https://www.getpostman.com/) - API Development Environment (Testing Docmentation)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.telstra.codechallenge.MicroServiceMain` class from your IDE.

- Download the zip or clone the Git repository.
- Unzip the zip file (if you downloaded one)
- Open Command Prompt and Change directory (cd) to folder containing pom.xml
- Open Eclipse 
   - File -> Import -> Existing Maven Project -> Navigate to the folder where you unzipped the zip
   - Select the project
- Choose the Spring Boot Application file (search for @SpringBootApplication)
- Right Click on the file and Run as Java Application

Alternatively you can use:

```shell
mvn spring-boot:run
```

### URLs

|  URL |  Method | Remarks |
|----------|--------------|--------------|
|`http://localhost:8080`                                         | GET | |
|`http://localhost:8080/search`                                  | GET | |
|`http://localhost:8080/search/5`                                | GET | |


